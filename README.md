![Logo](logo.jpg)

## Project Title
These are linux commands useful for running automation code on AWS linux server

## Authors
[@inwincible_shashank](https://gitlab.com/inwincible_shashank/)

## Purpose
To share and educate fellow QA's on useful linux commands when interacting with AWS linux server to run automation project build

## Download and Installation
1. Download & Install https://www.git-scm.com/download/win
2. Under Standalone Installer
3. Click on "64-bit Git for Windows Setup" link
4. .exe file will be downloaded
5. Then install the same on your windows machine
6. Then in your project explorer create a new empty folder
7. Left click and open gitbash here
8. Execute below commands                                              
```
  git clone https://gitlab.com/inwincible_shashank/linuxautomationcmds.git
```

## Project Explanation
1. README.md file is the start point for the download, installation and followed by git clone of the repo
2. The commands are segregated in list of lesson.md files to learn click on each file
3. For visual understanding screenshots are also added in Lesson.md files and they are referenced from 'Screenshots' directory so you can avoid it.
4. Directory "Practice Linux" is created to demonstarte/ practice these commands. 
*(Please note your cloned project might vary in terms of file/ folder present as during demonstartion of these commands folder/ files may have added/ deleted/ copied/ moved/ renamed/ etc.)*
5. Follow these commands as mentioned and it will work as instructed

## Interview Preparation/Revision
For quick interview preparation/ revision, use these summarized commands from file [Linux cmd Cheat Sheet](/LinuxCmdCheatSheet.md)

## Index
- [Lesson1: Basic cmds](Lessons/Lesson1-Basic%20cmds.md)
- [Lesson2: Creating files and nano editor](Lessons/Lesson2-Create%20files%20terminal%20editors%20(Part-1).md)
- [Lesson3: Editing content in vi editor](Lessons/Lesson3-Create%20files%20terminal%20editors%20(Part-2).md) 
- [Lesson4: List and it's attributes](Lessons/Lesson4-list%20and%20it's%20attribute%20commands.md)
- [Lesson5: Creating and accessing hidden files](Lessons/Lesson5-Creating%20and%20accessing%20hidden%20files.md)
- [Lesson6: Creating and removing folders](Lessons/Lesson6-Creating%20and%20removing%20folders.md)
- [Lesson7: Creating and removing zip files](Lessons/Lesson7-Creating%20zip%20files%20and%20removing%20files.md)
- [Lesson8: Read, copy content of files](Lessons/Lesson8-Read%20and%20copy%20content%20of%20files.md)
- [Lesson9: Using Head, Tail cmds](Lessons/Lesson9-Display%20content%20using%20head%20tail.md)
- [Lesson10: Various file, folder operations](Lessons/Lesson10-Copy,%20move,%20rename%20-%20files,%20folders.md)
- [Lesson11: Search using grep (Part-1)](Lessons/Lesson11-Search%20using%20'grep'%20(Part-1).md)
- [Lesson12: Search using grep (Part-2)](Lessons/Lesson12-Search%20using%20'grep'%20(Part-2).md)
- [Lesson13: Search using grep (Part-3)](Lessons/Lesson13-Search%20using%20'grep'%20(Part-3).md)
- [Lesson14: Search using find](Lessons/Lesson14-Search%20using%20'find'.md)
- [Lesson15: Sorting file contents](Lessons/Lesson15-sorting%20file%20contents.md)
- [Lesson16: Using compare and difference cmd](Lessons/Lesson16-Compare%20and%20Difference.md)
- [Lesson17: Filter content using AWK cmd](Lessons/Lesson17-Filter%20file%20content%20using%20'awk'%20cmd.md)

## Contact Me
- In the corresponding repository >> go to Plan >> Issues >> raise a New Issue
- inwincibleyou@gmail.com
- 8928885292

## Website
https://inwincibleshashank.in/