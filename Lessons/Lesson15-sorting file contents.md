## sorting file contents

| sort command               | cmd output                                  |
|----------------------------|---------------------------------------------|
| `sort user_activity.log` | sorted content A-Z *(in this case sorted based on Date-time stamp)* |
| `sort -r user_activity.log` | sorted content Z-A *(in this case sorted based on date-time stamp)* |
| `sort user_activity.log config.cfg pom.xml` | sorted content A-Z for multiple files |

---
Reference screenshots:
![sort command Screenshot](../Screenshots/Sortcmd.png)
---