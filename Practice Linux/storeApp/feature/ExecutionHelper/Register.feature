Feature: To demonstarte store and feed resposne data of a request in other feature file and request as a pre-requisite
# We have NOT added @sanity annotation for the feature file as this will only act as feeded for other request and we do not want the same in cucumber report 

Background:
#Declarations and file read of headers/ cookies
    * def fetchDataFromPrerequisiteFile = read('../other/prerequsite.json')
    * def getUrl = fetchDataFromPrerequisiteFile.config
    * def getHeaders = fetchDataFromPrerequisiteFile.actualHeaders

#Declarations and file read of 'Login.json' request body
* def getRequestBodyLogin = read('../request/requestBodyLogin.json')

@generateToken
Scenario: To verify 'access token' is generated and stored for further use
    Given url getUrl.storeBaseUrl + getUrl.typeAuthApi + getUrl.register
    * headers fetchDataFromPrerequisiteFile.actualHeaders
    And request getRequestBodyLogin.newUserDataCreation
    When method post
    Then status 201
    And print response
    And print responseHeaders
    And print responseCookies

    And assert responseStatus == 201

    #* fetchDataFromPrerequisiteFile.actualCookiesWithGeneratedCsrfToken.uid = responseCookies.uid.value
    * fetchDataFromPrerequisiteFile.actualToken.access_token = response.data.access_token
    * fetchDataFromPrerequisiteFile.actualToken.refresh_token = response.data.refresh_token

    * def storedTokenValues = fetchDataFromPrerequisiteFile.actualToken
    * print storedTokenValues